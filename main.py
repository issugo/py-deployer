# Copyright (c) 2024 MARGAIL Maurin
# Use of this source code is governed by a MIT license that can
# be found in the LICENSE file or at https://opensource.org/licenses/mit

"""
Entrypoint of py-deployer
"""
if __name__ == "__main__":
    print("PyCharm")
