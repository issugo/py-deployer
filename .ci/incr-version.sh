#!/usr/bin/env bash
set -euo pipefail

version="${1}"
major=0
minor=0
bugfix=0

# break down the version number into it's components
regex="([0-9]+).([0-9]+).([0-9]+)"
if [[ ${version} =~ ${regex} ]]; then
  major="${BASH_REMATCH[1]}"
  minor="${BASH_REMATCH[2]}"
  bugfix="${BASH_REMATCH[3]}"
fi

# check parameter to see which number to increment
if [[ "${2}" == "feature" ]]; then
  minor=$(echo "${minor}" + 1 | bc)
elif [[ "${2}" == "bug" ]]; then
  bugfix=$(echo "${build}" + 1 | bc)
elif [[ "${2}" == "major" ]]; then
  major=$(echo "${major}" + 1 | bc)
else
  echo "usage: ./${0}.sh [version-string] [major/feature/bug]"
  exit 0
fi

# new version number
echo "${major}.${minor}.${bugfix}"
