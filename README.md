[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)
[![linting: pylint](https://img.shields.io/badge/linting-pylint-yellowgreen)](https://github.com/pylint-dev/pylint)

auto version :
detect word in commit message : major, added, feat, feature, change, remove

conventional commit :
```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Simple Example:
```
Fixed: Bad variable substitution resulting in bug referenced in issue #4
Changed: python3 package instead of python2 for python dependencies
Added: psmisc package to pkg_installation variable
Removed: workstation.bashrc file had nothing to do here
```

Example :
```
fix: prevent racing of requests

Introduce a request id and a reference to latest request. Dismiss
incoming responses other than from latest request.

Remove timeouts which were used to mitigate the racing issue but are
obsolete now.

Reviewed-by: Z
Refs: #123
```
